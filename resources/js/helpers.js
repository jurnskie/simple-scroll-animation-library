export class Helpers{

    elementInViewport(el) {

        let top = el.offsetTop;
        let left = el.offsetLeft;
        let width = el.offsetWidth;
        let height = el.offsetHeight;

        while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        return (
            top >= window.pageYOffset &&
            left >= window.pageXOffset &&
            (top + height) <= (window.pageYOffset + window.innerHeight) &&
            (left + width) <= (window.pageXOffset + window.innerWidth)
        );
    }

    getTranslateX(element){
        let style = window.getComputedStyle(element);
        let matrix = new WebKitCSSMatrix(style.transform);
        return matrix.m41;
    }

    getTranslateY(element){
        let style = window.getComputedStyle(element);
        let matrix = new WebKitCSSMatrix(style.transform);

        return matrix.m42;
    }
}
