// This is all you.
import {scrollAnimations} from "./components/scroll-animations";

new scrollAnimations({
    scrollElement:'.header-title',
    direction:'from-top',
    speed: '.5s',
    waitForScroll: false
})

new scrollAnimations({
    scrollElement:'article.nieuws',
    direction:'from-left',
    speed: '.3s'
})

new scrollAnimations({
    scrollElement:'.service',
    direction:'from-bottom',
    speed: '1s'
})
