import {Helpers} from "../helpers";

export class scrollAnimations
{
    constructor(settings) {

        this.settings = settings;

        if( this.settings.scrollElement === undefined || this.settings.direction === undefined ){
            throw 'we need a scrollElement setting and/or direction setting';
        }
        this.scrollElement = this.settings.scrollElement;
        this.direction = this.settings.direction;

        this.initialTopPosition = this.settings.initialTopPosition || '-100vh';
        this.initialBottomPosition = this.settings.initialBottomPosition || '100vh';
        this.initialLeftPosition = this.settings.initialLeftPosition || '-100vw';
        this.initialRightPosition = this.settings.initialRightPosition || '100vw';

        this.speed = this.settings.speed || '.3s';
        this.easing = this.settings.easing || 'ease-in-out';

        if(this.settings['waitForScroll']!== undefined){
            this.waitForScroll = this.settings.waitForScroll;
        }else{
            this.waitForScroll = true;
        }

        this.id = Math.random().toString(36).substr(2, 5);

        this.init();
        this.helpers = new Helpers();
    }


    init(){

        let that = this,
            scrollElements = document.querySelectorAll(that.scrollElement),
            ww = window.outerWidth,
            st;

        if( ww < 768 ) return

        // add the required css to the dom based on unique id
        that.setupInitialCss(scrollElements);

        // trigger animation based on scroll event or not?
        if( !that.waitForScroll === false ){
            window.addEventListener('scroll', function(){
                st = this.scrollY;
                scrollElements.forEach(function (element){

                    if(that.helpers.elementInViewport(element) && st > 20){
                        that.animateElement(element);
                    }
                })
            });
        }else{
            scrollElements.forEach(function (element){
                that.animateElement(element);
            })
        }
    }

    setupInitialCss(scrollElements){
        let that = this;

       let scrollAnimationStyles= `
        <style id="animation-${that.id}">

            [data-animation=animation-${that.id}].to-be-animated{
                opacity: 0;
                position:relative;
                animation-duration: ${that.speed};
                animation-fill-mode: forwards;
                animation-timing-function: ${that.easing};
                will-change:transform,opacity;
            }

            [data-animation=animation-${that.id}].from-left{ transform: translateX(${that.initialLeftPosition}); }
            [data-animation=animation-${that.id}].from-left.animating{ animation-name:animate-from-left; }

            [data-animation=animation-${that.id}].from-right{ transform: translateX(${that.initialRightPosition}); }
            [data-animation=animation-${that.id}].from-right.animating{ animation-name:animate-from-right; }

            [data-animation=animation-${that.id}].from-top{ transform: translateY(${that.initialTopPosition}); }
            [data-animation=animation-${that.id}].from-top.animating{ animation-name:animate-from-top; }

            [data-animation=animation-${that.id}].from-bottom{ transform:translateY(100vh); }
            [data-animation=animation-${that.id}].from-bottom.animating{ animation-name:animate-from-bottom; }


            @keyframes animate-from-left {
                from{
                    opacity: 0;
                    transform: translateX(${that.initialLeftPosition});
                }
                to{
                  opacity: 1;
                  transform: translateX(0);
                }
            }

           @keyframes animate-from-right {
                from{
                    opacity: 0;
                    transform: translateX(${that.initialRightPosition});
                }
                to{
                  opacity: 1;
                  transform: translateX(0);
                }
            }

           @keyframes animate-from-top {
                from{
                    opacity: 0;
                    transform: translateY(${that.initialTopPosition});
                }
                to{
                  opacity: 1;
                  transform: translateY(0);
                }
          }
         @keyframes animate-from-bottom {
                from{
                    opacity: 0;
                    transform: translateY(${that.initialBottomPosition});
                }
                to{
                  opacity: 1;
                  transform: translateY(0);
                }
          }

        </style>`;

        // add style element to the body
        document.body.insertAdjacentHTML('afterbegin',scrollAnimationStyles);

        // if multiple elements add little delay for waterfall effect
        let delay = 0;
        scrollElements.forEach(function (animatableElement){
            delay+=.1;
            animatableElement.setAttribute('data-animation', 'animation-'+that.id);
            animatableElement.style.animationDelay = delay.toString() + 's';
            animatableElement.classList.add(that.direction, 'to-be-animated');
        });
    }
    // animate element by adding css class to it with keyframe animation
    animateElement(animatableElement){
        let that = this;
        animatableElement.classList.add(that.direction,'animating','animated');

        return animatableElement;
    }
}
